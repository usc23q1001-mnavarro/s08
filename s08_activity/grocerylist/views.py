from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from .models import GroceryItem

# Create your views here.

def index(request):
	groceryitem_list = GroceryItem.objects.all()
	template = loader.get_template("grocerylist/index.html")
	context = {
		'groceryitem_list': groceryitem_list
	}
	return HttpResponse(template.render(context, request))
	# return HttpResponse("Hello from the views.py file")


def groceryitem(request, groceryitem_id):
	response = "You are viewing the details of %s"
	return HttpResponse(response % groceryitem_id)